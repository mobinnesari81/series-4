package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HTMLParser {

    /*
    * this function will get a String and returns a Dom object
     */
    public static Map<String , String> setAttribute(String s)
    {
        String[] atts = s.split(" ");
        Map<String , String> answer = new HashMap<>();
        for (int i = 0 ; i < atts.length ; i++)
        {
            String obj = atts[i];
            int eqIndex = obj.indexOf('=');
            String key = "";
            String value = "";
            for (int j = 0 ; j < eqIndex ; j++)
            {
                key += obj.charAt(j);
            }
            for (int j = eqIndex+1 ; j < obj.length() ; j++)
            {
                value += obj.charAt(j);
            }
            answer.put(key , value);
        }
        return answer;
    }


    public static Node parse(String document) {
        // TODO implement this

        document = document.replaceAll("\n" , "").trim();
       String name = "";
       String s = "";
       Map<String , String> attributes = new HashMap<>();
       String insideString = "";
       String att = "";
       boolean isOpen =false;
       ArrayList<Node> Nodes = new ArrayList<>();
       for (int i = 0 ; i < document.length() ; i++)
       {
           if (document.charAt(i) == '<')
           {
               if (document.charAt(i+1) != '/')
               {
                   isOpen = true;
                   i++;
                   while (document.charAt(i) != ' ' && document.charAt(i) != '>')
                   {
                       s += document.charAt(i);
                       i++;
                   }
                   name = s;
                   s = "";
                   if (document.charAt(i) == ' ')
                   {
                       i++;
                       int endIndex = document.indexOf('>' , i);
                       att = document.substring(i , endIndex);
                       attributes = setAttribute(att);
                       i = endIndex+1;
                   }
                   else
                   {
                       i++;
                   }
                   int endIndexTag = document.indexOf("</"+name+">" , i);
                   int j = i;
                   while (j != endIndexTag)
                   {
                       insideString += document.charAt(j);
                       j++;
                   }
                   Nodes.add(new Node(name , attributes , insideString));
                   name = "";
                   attributes.clear();
                   insideString = "";
                   i--;
               }
               else
               {
                   i++;
                   i++;
                   while (document.charAt(i) != ' ' && document.charAt(i) != '>')
                   {
                       name += document.charAt(i);
                       i++;
                   }
                   int k = document.indexOf("<"+name);
                   if (k == -1)
                   {
                       if (document.charAt(i) == ' ')
                       {
                           String str = "";
                           while (document.charAt(i) != '>')
                           {
                               str += document.charAt(i);
                               i++;
                           }
                           attributes = setAttribute(str);
                       }
                       insideString = "";
                       Nodes.add(new Node(name , attributes , insideString));
                       name = "";
                   }
                   else
                   {
                       name = "";
                   }
               }
           }
       }

       for (Node n : Nodes)
       {
           n.setChildren(stringToChildren(n.getStringInside()));
       }


       return Nodes.get(0);
    }

    public static ArrayList<Node> stringToChildren(String document)
    {
        if (document == null || document == "")
        {
            return null;
        }
        ArrayList<Node> children = new ArrayList<>();
        document = document.replaceAll("\n" , "").trim();
        boolean isOpened = false;
        int k = document.indexOf("<");
        if (k == -1)
        {
            return null;
        }
        for (int i = 0 ; i < document.length() ; i++)
        {
            String name = "";
            Map<String , String> attrs = new HashMap<>();
            String insideString = "";
            if (document == "")
                return null;
            if (document.charAt(i) == '<')
            {
                if (document.charAt(i+1) == '/')
                {
                    i++;
                    i++;
                    if (isOpened)
                    {
                        isOpened = false;
                        i = document.indexOf('>' , i);
                    }
                    else
                    {
                        while (document.charAt(i) != '>' && document.charAt(i) != ' ')
                        {
                            name += document.charAt(i);
                            i++;
                        }
                        if (document.charAt(i) == ' ')
                        {
                            i++;
                            String at = document.substring(i , document.indexOf('>' , i));
                            i = document.indexOf('>' , i);
                            attrs = setAttribute(at);
                        }
                        insideString = "";
                        Node child = new Node(name , attrs , insideString);
                        child.setChildren(stringToChildren(child.getStringInside()));
                        children.add(child);
                    }
                }
                else {
                    i++;
                    while (document.charAt(i) != ' ' && document.charAt(i) != '>')
                    {
                        name += document.charAt(i);
                        i++;
                    }
                    if (document.charAt(i) == ' ')
                    {
                        String str = "";
                        while (document.charAt(i) != '>')
                        {
                            str+=document.charAt(i);
                            i++;
                        }
                        attrs = setAttribute(str);
                    }
                    i++;
                    insideString = document.substring(i , document.indexOf("</"+name+">",i));
                    i = document.indexOf("</"+name+">",i);
                    i = document.indexOf('>' , i);
                    Node child = new Node(name , attrs , insideString);
                    child.setChildren(stringToChildren(child.getStringInside()));
                    children.add(child);
                }
            }
        }
        return children;
    }

    public static Node switcher(ArrayList<Node> nodes , String name)
    {
        for (Node n : nodes)
        {
            if (n.getName().equals(name))
            {
                return n;
            }
        }
        return null;
    }


    public static ArrayList<String> getKinds(String input)
    {
        ArrayList<String> answer = new ArrayList<>();
        input = input.replaceAll("\n" , "").trim();
        String s = "";
        input = input.replaceAll("\n" , "").trim();
        for (int i = 0 ; i < input.length() ; i++)
        {
            if (input.charAt(i) == '<')
            {
                if (input.charAt(i+1) == '/')
                {
                    i++;
                    while (input.charAt(i) != '>')
                    {
                        s += input.charAt(i);
                        i++;
                    }
                    answer.add(s);
                    s = "";
                }
                else
                {
                    i++;
                    while(input.charAt(i) != '>' && input.charAt(i) != ' ')
                    {
                        s += input.charAt(i);
                        i++;
                    }
                    answer.add(s);
                    s = "";
                }
            }
        }
        return answer;
    }




    /*
    * a function that will return string representation of dom object.
    * only implement this after all other functions been implemented because this
    * impl is not required for this series of exercises. this is for more score
     */
    public static String toHTMLString(Node root) {
        // TODO implement this for more score
        return null;
    }
}
