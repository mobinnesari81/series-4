package sbu.cs.parser.html;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Tester {
    public static void main(String[] args)
    {
        String document = "<html>\n" +
        "<body>\n" +
                "\n" +
                "<h2>Width and Height Attributes</h2>\n" +
                "\n" +
                "<p>The width and height attributes of the img tag, defines the width and height of the image:</p>\n" +
                "\n" +
                "<img src=\"img_girl.jpg\" width=\"500\" height=\"600\">\n" + "</img>" +
                "\n" +
                "</body>\n" +
                "</html>";
        parse(document);

    }


    public static Node parse(String document) {
        // TODO implement this
        document = document.replaceAll("\n" , "").trim();
        String name = "";
        String s = "";
        Map<String , String> attributes = new HashMap<>();
        String insideString = "";
        String att = "";
        ArrayList<Node> Nodes = new ArrayList<>();
        for (int i = 0 ; i < document.length() ; i++)
        {
            if (document.charAt(i) == '<')
            {
                if (document.charAt(i+1) != '/')
                {
                    i++;
                    while (document.charAt(i) != ' ' && document.charAt(i) != '>')
                    {
                        s += document.charAt(i);
                        i++;
                    }
                    name = s;
                    s = "";
                    if (document.charAt(i) == ' ')
                    {
                        i++;
                        int endIndex = document.indexOf('>' , i);
                        att = document.substring(i , endIndex);
                        attributes = setAttribute(att);
                        i = endIndex+1;
                    }
                    else
                    {
                        i++;
                    }
                    int endIndexTag = document.indexOf("</"+name+">" , i);
                    int j = i;
                    while (j != endIndexTag)
                    {
                        insideString += document.charAt(j);
                        j++;
                    }
                    Nodes.add(new Node(name , attributes , insideString));
                    System.out.println(Nodes.get(Nodes.size()-1));
                    name = "";
                    attributes.clear();
                    insideString = "";
                    System.out.println(Nodes.get(Nodes.size()-1));
                    i--;
                }
            }
        }
        System.out.println("Nodes: ");
        /*for (int i = 0 ; i < Nodes.size() ; i++)
        {
            System.out.println(Nodes.get(i));
        }
        Node answer = new Node(name , attributes , insideString);
        answer.addChild(parse(insideString));*/
        return null;
    }


    public static Map<String , String> setAttribute(String s)
    {
        String[] atts = s.split(" ");
        Map<String , String> answer = new HashMap<>();
        for (int i = 0 ; i < atts.length ; i++)
        {
            String obj = atts[i];
            int eqIndex = obj.indexOf('=');
            String key = "";
            String value = "";
            for (int j = 0 ; j < eqIndex ; j++)
            {
                key += obj.charAt(j);
            }
            for (int j = eqIndex+1 ; j < obj.length() ; j++)
            {
                value += obj.charAt(j);
            }
            answer.put(key , value);
        }
        return answer;
    }

    public ArrayList<String> getKinds(String input)
    {
        ArrayList<String> answer = new ArrayList<>();
        String s = "";
        input = input.replaceAll("\n" , "").trim();
        for (int i = 0 ; i < input.length() ; i++)
        {
            if (input.charAt(i) == '<')
            {
                if (input.charAt(i+1) == '/')
                {
                    i++;
                    while (input.charAt(i) != '>')
                    {
                        s += input.charAt(i);
                        i++;
                    }
                    answer.add(s);
                    s = "";
                }
                else
                {
                    i++;
                    while(input.charAt(i) != '>' && input.charAt(i) != ' ')
                    {
                        s += input.charAt(i);
                        i++;
                    }
                    answer.add(s);
                    s = "";
                }
            }
        }
        return answer;
    }

    public ArrayList<String> getInsideStrings(String input)
    {
        ArrayList<String> answer = new ArrayList<>();
        String s = "";
        input = input.replaceAll("\n" , "").trim();
        for (int i = 0 ; i < input.length() ; i++)
        {
            if (input.charAt(i) == '<')
            {
                if (input.charAt(i+1) != '/')
                {
                    String name = "";
                    i++;
                    while (input.charAt(i) != ' ' && input.charAt(i) != '>')
                    {
                        name += input.charAt(i);
                        i++;
                    }
                    System.out.println(name);
                    if (input.charAt(i) == ' ')
                    {
                        while (input.charAt(i) != '>')
                        {
                            i++;
                        }
                        int index = input.indexOf("</"+name+">" , i);
                        System.out.println(index);
                        int j = i+1;
                        for (j = i+1 ; j < index; j++)
                        {
                            s+= input.charAt(j);
                        }
                        answer.add(s);
                        s = "";
                    }
                    else
                    {
                        int index = input.indexOf("</"+name+">" , i);
                        System.out.println(index);
                        int j = i+1;
                        for (j = i+1 ; j < index; j++)
                        {
                            s+= input.charAt(j);
                        }
                        answer.add(s);
                        s = "";
                    }
                }
            }
        }
        return answer;
    }

    public Map<String , String> getAttributes(String input , String target)
    {
        ArrayList<String[]> answer = new ArrayList<String[]>();
        ArrayList<String> kinds = getKinds(input);
        for (int i = 0 ; i < kinds.size() ; i++)
        {
            if (kinds.get(i).charAt(0) == '/')
            {
                kinds.remove(new String(kinds.get(i)));
            }
            i--;
        }
        String s = "";
        input = input.replaceAll("\n" , "").trim();
        for (int i = 0 ; i < input.length() ; i++)
        {
            if (input.charAt(i) == '<')
            {
                if (input.charAt(i+1) != '/')
                {
                    while (input.charAt(i) != ' ' && input.charAt(i) != '>')
                    {
                        i++;
                    }
                    if (input.charAt(i) == '>')
                    {
                        String[] oneObj = new String[0];
                        answer.add(oneObj);
                    }
                    else
                    {
                        while (input.charAt(i) != '>')
                        {
                            s += input.charAt(i);
                            i++;
                        }
                        String[] oneObj = s.split(" ");
                        answer.add(oneObj);
                        s = "";
                    }
                }
            }
        }
        Map<String , String> ans = new HashMap<>();
        for (int i = 0 ; i < answer.size(); i++)
        {
            if (kinds.get(i) == target)
            {
                String[] obj = answer.get(i);
                for (int j = 0 ; j < obj.length ; j++)
                {
                    String p = obj[j];
                    if (p.contains("="))
                    {
                        String key = p.substring(0,p.indexOf('='));
                        String value = p.substring(p.indexOf('=')+1,p.length());
                        ans.put(key,value);
                    }
                }
            }

        }
        return ans;
    }
}
