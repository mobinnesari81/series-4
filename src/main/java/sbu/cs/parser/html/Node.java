package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {

    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    private String name;
    private Map<String , String> attributes;
    private ArrayList<Node> children;
    private String insideString;

    public Node(String name , Map<String , String> attributes , String insideString)
    {
        this.name = name;
        this.attributes = attributes;
        this.insideString = insideString;
    }



    @Override
    public String getStringInside() {
        // TODO implement this
        if (this.insideString == "")
        {
            return null;
        }
        return this.insideString;
    }

    public void setChildren(ArrayList<Node> children)
    {
        this.children = children;
    }

    /*
    *
     */
    @Override
    public List<Node> getChildren() {
        return this.children;
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        // TODO implement this
        String s = this.attributes.get(key);
        if (s == null)
        {
            return s;
        }
        s = s.substring(1,s.length()-1);
        return s;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString()
    {
        String answer = "";
        answer += "Node=[name:"+this.name+", InsideString="+this.insideString+"]\n";
        answer += "Attributes:\n";
        String[] keys = this.attributes.keySet().toArray(new String[0]);
        String[] values = this.attributes.values().toArray(new String[0]);
        for (int i = 0 ; i < this.attributes.size() ; i++)
        {
            answer += "<"+keys[i]+","+values[i]+"> ";
        }

        answer += '\n';
        answer += "Children:";
        if (this.children != null) {
            for (Node n : this.children) {
                answer += n.getName() + " ";
            }
        }

        return answer;
    }

    public void addChild(Node child) {
        this.children.add(child);
    }
}
