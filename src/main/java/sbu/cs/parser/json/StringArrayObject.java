package sbu.cs.parser.json;

import java.util.ArrayList;

public class StringArrayObject extends JsonObject{
    private ArrayList<String> value;
    public StringArrayObject(String key)
    {
        super(key);
        this.value = new ArrayList<String>();
    }
    public StringArrayObject(String key , ArrayList<String> value)
    {
        super(key);
        this.value = value;
    }
    public void setValue(ArrayList<String> value)
    {
        this.value = value;
    }
    public ArrayList<String> getValue()
    {
        return this.value;
    }
}
