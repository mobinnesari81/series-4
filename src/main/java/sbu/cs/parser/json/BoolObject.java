package sbu.cs.parser.json;

public class BoolObject extends JsonObject{
    private boolean value;
    public BoolObject(String key)
    {
        super(key);
    }
    public BoolObject(String key , boolean value) {
        super(key);
        this.value = value;
    }
    public void setValue(boolean value)
    {
        this.value = value;
    }
    public boolean getValue()
    {
        return this.value;
    }
}
