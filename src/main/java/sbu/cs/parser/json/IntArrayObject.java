package sbu.cs.parser.json;

import java.util.ArrayList;

public class IntArrayObject extends JsonObject{
    private ArrayList<Integer> value;
    public IntArrayObject(String key)
    {
        super(key);
        this.value = new ArrayList<Integer>();
    }
    public IntArrayObject(String key , ArrayList<Integer> value)
    {
        super(key);
        this.value = value;
    }
    public void setValue(ArrayList<Integer> value)
    {
        this.value = value;
    }
    public ArrayList<Integer> getValue()
    {
        return this.value;
    }
}
