package sbu.cs.parser.json;

public class JsonObject {
    private String key;

    public JsonObject(String key)
    {
        this.key = key;
    }

    public String getKey()
    {
        return this.key;
    }
    public void setKey(String key)
    {
        this.key = key;
    }

}
