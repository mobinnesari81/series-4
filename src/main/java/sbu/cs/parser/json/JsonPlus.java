package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonPlus {
    private ArrayList<String> keys;
    private ArrayList<JsonObject> objects;
    public JsonPlus(ArrayList<String> keys , ArrayList<String> vals)
    {
        this.keys = keys;
        for (int i = 0 ; i < keys.size() ; i++)
        {
            String val = vals.get(i);
            if (Character.isDigit(val.charAt(0)))
            {
                IntObject object = new IntObject(keys.get(i) ,  Integer.parseInt(val));
                objects.add(object);
            }
            else if (val.equals("true"))
            {
                BoolObject object = new BoolObject(keys.get(i) , true);
                objects.add(object);
            }
            else if (val.equals("false"))
            {
                BoolObject object = new BoolObject(keys.get(i), false );
                objects.add(object);
            }
            else if (!Character.isDigit(val.charAt(0)))
            {
                StringObject object = new StringObject(keys.get(i) , val);
                objects.add(object);
            }
            else if (val.charAt(0) == '[')
            {
                if (val.charAt(1) == '\"')
                {
                    String[] strings = val.substring(1 , val.length()-1).split(",");
                    ArrayList<String> vls = new ArrayList<String>();
                    for (int j = 0 ; j < strings.length ; j++)
                    {
                        vls.add(strings[j].substring(1,strings[j].length()-1));
                    }
                    StringArrayObject object = new StringArrayObject(keys.get(i) , vls);
                    objects.add(object);
                }
                else
                {
                    String[] strings = val.substring(1,val.length()-1).split(",");
                    ArrayList<Integer> vls = new ArrayList<Integer>();
                    for (int j = 0 ; j < strings.length ; j++)
                    {
                        vls.add(Integer.parseInt(strings[j]));
                    }
                    IntArrayObject object = new IntArrayObject(keys.get(i) , vls);
                    objects.add(object);
                }
            }
        }
    }
}
