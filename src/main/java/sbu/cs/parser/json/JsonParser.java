package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
        // TODO implement this
        data = data.replaceAll("\\s" , "");
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();
        boolean isClose = false;
        boolean isKey = true;
        String s = "";
        for (int i = 1 ; i < data.length()-1 ; i++)
        {
            //System.out.println("char="+data.charAt(i)+", s = " + s + ", isClose = "+isClose+", isKey=" + isKey + keys + values);
            if (data.charAt(i) == '\"')
            {
                if (isClose)
                {
                    if (isKey)
                    {
                        keys.add(s);
                    }
                    else
                    {
                        values.add(s);
                    }
                    isClose = false;
                    s = "";
                }
                else
                {
                    isClose = true;
                }
            }
            else if (data.charAt(i) == ':')
            {
                isKey = false;
            }
            else if (data.charAt(i) == ',')
            {
                isKey = true;
            }
            else if (data.charAt(i) == 'n')
            {
                if (isClose == false)
                {
                    values.add("null");
                    i += 3;
                }
                else
                {
                    s += 'n';
                }

            }
            else if (data.charAt(i) == 't')
            {
                if (isClose == false)
                {
                    values.add("true");
                    i += 4;
                }
                else
                {
                    s += 't';
                }

            }
            else if (data.charAt(i) == 'f')
            {
                if (isClose == false)
                {
                    values.add("false");
                    i += 5;
                }
                else
                {
                    s += 'f';
                }

            }

            else if (data.charAt(i) == '[')
            {
                while (data.charAt(i) != ']')
                {
                    s+=data.charAt(i);
                    if (data.charAt(i) == ',')
                    {
                        s+=" ";
                    }
                    i++;

                }
                s+=']';
                values.add(s);
                s = "";
                isKey = true;
            }
            else if (Character.isDigit(data.charAt(i)))
            {
                s += data.charAt(i);
                if (data.charAt(i+1) == ',' || data.charAt(i+1) == '}' || data.charAt(i) == ':')
                {
                    if (isKey)
                    {
                        keys.add(s);
                        s = "";
                    }
                    else
                    {
                        values.add(s);
                        s = "";
                    }
                }
            }
            else
            {
                s += data.charAt(i);
            }
        }
        Json answer = new Json(keys , values);
        System.out.println(keys + " " + values);
        return answer;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        String answer = "{";
        for (int i = 0 ; i < data.getDataLength(); i++)
        {
            answer += data.getKeyAtIndex(i);
            answer += ":";
            answer += data.getStringValue(data.getKeyAtIndex(i));
            if (i != data.getDataLength()-1)
            {
                answer += ',';
            }
        }
        answer += '}';
        return answer;
    }
}
