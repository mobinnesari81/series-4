package sbu.cs.parser.json;

public class StringObject extends JsonObject{
    private String value;
    public StringObject(String key)
    {
        super(key);
    }
    public StringObject(String key , String value)
    {
        super(key);
        this.value =value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    public String getValue()
    {
        return this.value;
    }
}
