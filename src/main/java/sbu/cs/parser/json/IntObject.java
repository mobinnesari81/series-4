package sbu.cs.parser.json;

public class IntObject extends JsonObject{
    private int value;
    public IntObject(String key) {
        super(key);
        this.value = 0;
    }
    public IntObject(String key , int value)
    {
        super(key);
        this.value = value;
    }
    public int getValue()
    {
        return this.value;
    }
    public void setValue(int value)
    {
        this.value = value;
    }
}
