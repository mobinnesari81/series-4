package sbu.cs.parser.json;

import java.util.ArrayList;

public class Json implements JsonInterface {
    private ArrayList<String> keys;
    private ArrayList<String> values;

    public Json(ArrayList<String> keys , ArrayList<String> values)
    {
        this.keys = keys;
        this.values = values;
    }

    @Override
    public String getStringValue(String key) {
        // TODO implement this
        for (int i = 0 ; i < this.keys.size() ; i++)
        {
            if (keys.get(i).equals(key))
            {
                return values.get(i);
            }
        }
        return null;
    }
    public int getDataLength()
    {
        return keys.size();
    }
    public String getKeyAtIndex(int i)
    {
        return keys.get(i);
    }
}
